var jsrow = {

	// Options start here
	table_selector    : '.ranking-table:eq(0)',
	max_players       : 6,
	max_total         : 50000,
	players_separator : ', ',
	tpl: {
		count  : '[#n/#max_players]',
		player : '#name (#salary)',
		total  : '#total (<span class="#cls">#diff</span>)',
		row    : '<span class="jsrow-title">TEAMBUILDER:</span> Total #total_with_diff #count: #players <span class="jsrow-clear">CLR</span>',
	},
	// Options end here

	$table: null,
	init: function() {
		this.$table = jQuery(this.table_selector);
		this.bindClearBtn();
		return this;
	},
	addHeaderRow: function() {
		$( '.sticky-header' ).stickyTableHeaders( 'destroy' );
		this.$table.find( 'thead tr:last' ).after( '<tr><td colspan="25" id="jsrow"></td></tr>' );
		$( '.sticky-header' ).stickyTableHeaders();
		return this;
	},
	addCheckboxes: function() {
		var $names_TDs = this.$table.find( 'tbody td:first-child' );
		$names_TDs.each( function() {
			var $this = $( this );
			var $checkbox = $( '<input type="checkbox">' );
			$checkbox
				.on( 'change', jsrow.updatePlayerRow )
				.on( 'change', jsrow.updateHeaderRow )
				.appendTo( $this )
			;
		} );
		return this;
	},
	bindClearBtn: function() {
		$( document ).on( 'click', '.jsrow-clear', function() {
			jsrow.$table.find( 'tr' ).filter( '.jsrow-checked' ).find( ':checkbox' ).click();
		} );
	},
	updatePlayerRow: function() {
		var $this = $( this );
		$this
			.closest( 'tr' )
			.toggleClass( 'jsrow-checked', $this.is( ':checked' ) )
		;
		// Uncheck this is max_players is exceeded
		var selectedPlayersRows = jsrow.$table.find( 'tr' ).filter( '.jsrow-checked' );
		if ( selectedPlayersRows.length > jsrow.max_players ) {
			$this.click();
		}
	},
	updateHeaderRow: function() {
		var headerRow = $( '#jsrow' );
		var newText = '';
		var selectedPlayersRows = jsrow.$table.find( 'tr' ).filter( '.jsrow-checked' );
		var count = jsrow.tpl.count
			.replace( '#n' , selectedPlayersRows.length )
			.replace( '#max_players' , jsrow.max_players )
		;
		var total = 0;
		var diff;
		var cls;
		var total_with_diff;
		var players = [];

		selectedPlayersRows.each( function() {
			var $this = $( this );
			var player_name = $this.find( 'td:eq(0)' ).text();
			var draft_kings_salary = $this.find( 'td:eq(1)' ).text();
			if ( ! draft_kings_salary.match(/^\d+$/) ) {
				draft_kings_salary = $this.find( 'td:eq(2)' ).text();
			}
			players.push( jsrow.tpl.player
				.replace( '#name' , player_name.replace( ' (P/T/Y)', '' ) )
				.replace( '#salary' , jsrow.numberWithCommas( draft_kings_salary ) )
			);
			total += parseInt(draft_kings_salary);
		} );

		players = players.join( jsrow.players_separator )
		diff = jsrow.max_total - total;
		cls = diff < 0 ? 'jsrow-total-overage' : 'jsrow-total-shortage';

		total_with_diff = jsrow.tpl.total
			.replace( '#total' , jsrow.numberWithCommas( total ) )
			.replace( '#cls' , cls )
			.replace( '#diff' , jsrow.numberWithCommas( Math.abs( diff ) ) )
		;

		row = jsrow.tpl.row
			.replace( '#count', count )
			.replace( '#players', players )
			.replace( '#total_with_diff', total_with_diff )
		;

		headerRow.html( row );
	},
	numberWithCommas: function (x) {
		// http://stackoverflow.com/a/2901298/358906
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
};

jQuery( document ).ready( function( $ ) {
	jsrow
		.init()
		.addHeaderRow()
		.addCheckboxes()
		.updateHeaderRow()
	;
} );
