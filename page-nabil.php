<?php
/**
 * This is a copy of the GOLFstats IQ file for Nabil.
 * ecp 6/25/2016
 *
 * @package  GolfStats
 * @since    1.0.0
 */

// functions

function player_picture($player) {
if ($player <> '') $img = '<div class="ranking_photo"><img src="/~golfstat/wp-content/themes/golfstats/images/headshots/' . $player . '" height="16" width="16"></div>';
else $img = '' ;
return $img;
}

function player_bio ($pname, $tname, $tyear) {
$tname = str_replace("&amp;","%26",$tname) ;
$plink = $pname
.' (<a href="http://www.golfstats.com/search/?player='.$pname.'&career=&chart=&box=&tour=&tournament=&yr=&submitted=go" title="Link to Player Profile">P</a>'
.'/<a href="http://www.golfstats.com/search/?player='.$pname.'&career=&chart=&box=&tour=&tournament='.$tname.'&yr=&submitted=go" title="Link to Player Results for '.$tname.'">T</a>'
.'/<a href="http://www.golfstats.com/search/?player='.$pname.'&career=&chart=&box=&tour=&tournament=&yr='.($tyear).'&submitted=go" title="Link to Player Results for Tour Year '.($tyear).' (YTD)">Y</a>)' ;
return $plink ;
}

function MySQLQuery($query) {
    if(connection_status() != CONNECTION_NORMAL){ exit("CONNECTION FAILED"); }
    $result = mysql_query($query) or die (mysql_error()."\r\n");
    return $result;
}

// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}


// Force full width layout
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

// Enables user saved searches for this page
ja_save_search();

// Remove default loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

function ja_predict_content() { 

if ($_GET['nohdr'] <> 1) {
	
	ja_section_wrap( 'open');
	$pagetitle = 'GOLFstats IQ - Prior Tournaments' ;
	//echo '<h1 class="entry-title-snug zero-margin">'.$pagetitle.'</h1>' ;

	echo '<div class="predict_menu">' ;

	echo '<div class="alert alert-iq alert-iq-xl">
	<a href="http://www.golfstats.com/nabil/">
	<img src="http://www.GOLFstats.com/wp-content/themes/golfstats/images/iq-logo1.png"></a>&nbsp;
	Welcome to GOLFstats <span style="font-weight: bold;color:#ED9601">iQ</span> - Intelligence for Fantasy Golf</div>' ;

	echo '<table class="table">
<tr><td width="70%"><b>Daily Fantasy Sports</b> are increasingly in popularity, with sites like <a href="http://partners.fanduel.com/processing/clickthrgh.asp?btag=a_6716b_2">FanDuel</a> and  
<a href="http://partners.draftkings.com/SH17M">Draft Kings</a> leading the way. <a href="http://partners.draftkings.com/SH17M">DraftKings</a> now offers <u>WEEKLY GOLF GAMES</u> as well!</b>. 
<br><br><b>To sign up to play in these games, just click on the banner to the right!</b>
<br><br>To help you with your picks, we provide the <b>GOLFstats</b> <span style="font-weight: bold;color:#ED9601">iQ</span> reports below. 

<br><br>Any comments, just let us know via <a href="mailto:support@golfstats.com">support@golfstats.com</a>. <b>Thank you!</b></td><td width="30%">
<a href="http://partners.draftkings.com/aff_c?offer_id=832&aff_id=262300&file_id=4066" target="_blank"><img src="https://media.go2speed.org/brand/files/draftkings/832/300x250_PGA_Generic.png" width="300" height="250" border="0" /></a>
</td></tr></table>' ;

//<br><br>
//<b>Prior Tournaments:</b>This report will show you the best players over the most recent weeks on Tour and years back in the specific Tournament. You control how many weeks to look back. Plus many other options. All intended to give you the best data and research to make your picks.
//<br><br>
//<b>Horses for Courses:</b>This report shows you the Fields performance on the Tournament Course (over any period of years, and any tournament that played on the course), or ANY Course you enter. You can sort the report by column performance value (avg birdies, eagles, driving distance, putts, etc)
//<br><br>
//<b>Course Stats:</b>This report shows you the performance stats for each course used on the PGA Tour or Majors, or for a specific year, course or tournament starting from 1990. You can sort the report by column performance value (avg birdies, eagles, driving distance, putts, etc)
//<br><br>

	echo '<table class="table" width="80%"><tr>
	<td colspan="3" STYLE="text-align: center">
	<div class="alert alert-announce-iq">
	<a href="http://www.golfstats.com/category/draftkings/">DraftKings Posts, Advice and Picks</a>
	</div>
	</td>
	</tr>
	
	<tr>	
	<td width="33%" STYLE="text-align: center"><div class="alert alert-announce-iq">
	<a href="http://www.golfstats.com/nabil/?tid=1">Prior Tournaments</a>
	<SPAN STYLE="color: gray; font-size: 9pt;"><br>View the fields results over the past N weeks on Tour, and the past Y years in this weeks tournament.</span>
	</div></td>
	
	<td width="33%" STYLE="text-align: center"><div class="alert alert-announce-iq"><a href="http://www.golfstats.com/horses/">Horses for Courses</a>
	<SPAN STYLE="color: gray; font-size: 9pt;"><br>View the performance stats of the tournament field on the tournament course, or any course</span>
	</div></td>
	
	<td width="33%" STYLE="text-align: center"><div class="alert alert-announce-iq"><a href="http://www.golfstats.com/course-stats/">Course Stats</a>
	<SPAN STYLE="color: gray; font-size: 9pt;"><br>Compare the perf stats amongst tournament courses (courses with most birdies, lowest score avg, etc)</span>
	</div></td>

	
	</tr></table>' ;

	echo '</div>' ;
	
	ja_section_wrap( 'close' );
	
}
	
	if ($_GET['tid'] > 0 && $_GET['nohdr'] <> 1) {
	
	ja_section_wrap( 'open');
	$pagetitle = 'GOLFstats IQ - Prior Tournaments' ;
	echo '<h1 class="entry-title-snug zero-margin">'.$pagetitle.'</h1>' ;
	echo '<div class="predict_menu">' ;
	
	//echo '<br><div class="alert alert-announce">Use the tool below to help you make your picks</div>' ;

	echo '<form method="GET" action="'.get_permalink().'">' ;
	
	// start table
	echo '<table class="table table-striped-even table-bordered">' ;
	
	echo '<tr><td>' ;
	//echo '<div class="alert alert-info">Note: DraftKings salaries have been updated as of 8amPDT, 5/25. Sorry for the delay. There was a problem on the DK side.</div><br>' ;
	echo '<label for=Tournament"><b>Select a Tournament: </b></label>' ;
	echo '<select name ="tid" id="tournament-select" style="width:424px;padding:0px;" class="remove-select2 reset-select" data-placeholder="Pick a Tournament">';
	//echo '<option value="">Pick a Tournament</option>';
	// get last updated
	$dblink = mysql_connect( 'localhost', 'golfstat_query', 'gstt2qry' );
	mysql_select_db( "golfstat_main", $dblink );
	
	$q1 = 'SELECT * FROM field ORDER BY id ASC' ;
	$results = mysql_query($q1) or die (mysql_error()." 11 \r\n");

	while ( $r1 = mysql_fetch_assoc( $results ) ) {
		// make PGA TOUR event the selected on
		if ($r1['tour'] == 'PGA' || $r1['tour'] == 'BOTH' || $r1['tour'] == 'Majors') $selected_txt = 'selected' ; else $selected_txt = '' ;
		// build option list
		$olist .= '<option value="' . $r1['tid']. '" ' .$selected_txt.' >' . $r1['name'].': '.$r1['dates']  . '</option>' ;
		}
	echo $olist;
	echo '</select>';
	echo '<br><SPAN STYLE="color: gray; font-size: 10pt; margin-left: 16px">Tournaments listed are ones we have a field for. Fields are uploaded by 4pm EST on Mondays.</span>' ;
	echo '</td></tr>';
	
	echo '<tr><td>' ;
	echo '<label>OPTIONS (select any, or leave as is to accept defaults. Click GO at the bottom to generate report)</label>' ;
	echo '</td></tr>';
	
	echo '<tr><td>' ;
	echo '<label for="years">1. How many years to go back for this tournament? </label>' ;
	echo '<select name ="years" id="years-select" style="width:48px;padding:0px;" class="remove-select2 reset-select" data-placeholder="Number of Years">';
	//echo '<option value="">Number of Years</option>';
	echo '<option value="1">1</option>' ;
	echo '<option value="2">2</option>' ;
	echo '<option value="3">3</option>' ;
	echo '<option value="4" selected>4</option>' ;
	echo '<option value="5">5</option>' ;
	echo '<option value="6">6</option>' ;
	echo '<option value="7">7</option>' ;
	echo '<option value="8">8</option>' ;
	echo '<option value="9">9</option>' ;
	echo '<option value="10">10</option>' ;
	echo '</select>';
	echo '</td></tr>';
	
	echo '<tr><td>' ;
	echo '<label for="weeks">2. How many weeks to go back for this season? </label>' ;
	echo '<select name ="weeks" id="weeks-select" style="width:48px;padding:0px;" class="remove-select2 reset-select" data-placeholder="Number of Years">';
	//echo '<option value="">Number of Weeks</option>';
	echo '<option value="1">1</option>' ;
	echo '<option value="2">2</option>' ;
	echo '<option value="3">3</option>' ;
	echo '<option value="4" selected>4</option>' ;
	echo '<option value="5">5</option>' ;
	echo '<option value="6">6</option>' ;
	echo '<option value="7">7</option>' ;
	echo '<option value="8">8</option>' ;
	echo '<option value="9">9</option>' ;
	echo '<option value="10">10</option>' ;
	echo '</select>';
	echo '<br><SPAN STYLE="color: gray; font-size: 9pt; margin-left: 16px">This is relative to the week of the tournament. We will include PGA TOUR events and European Tour events if applicable. You can also use this option to research previous tournaments IQ</span>' ;
	echo '<br><SPAN STYLE="color: gray; font-size: 9pt; margin-left: 16px">and compare to results. The actual tournaments used are listed at the bottom of the page</span>' ;
	echo '</td></tr>';
	
	echo '<tr><td>' ;
	echo '<label for="rounds">3. Minimum number of rounds played to list? </label>' ;
	echo '<select name ="rounds" id="rounds-select" style="width:48px;padding:0px;" class="remove-select2 reset-select" data-placeholder="Number of Rounds">';
	echo '<option value="0" selected>0</option>' ;
	echo '<option value="2">2</option>' ;
	echo '<option value="4">4</option>' ;
	echo '<option value="5">5</option>' ;
	echo '<option value="6">6</option>' ;
	echo '<option value="8">8</option>' ;
	echo '<option value="12">12</option>' ;
	echo '<option value="16">16</option>' ;
	echo '</select>';
	echo '<br><SPAN STYLE="color: gray; font-size: 10pt; margin-left: 16px">Only players with this number of rounds or greater, for previous weeks on tour, will be listed. Default (0) lists all.</span>' ;
	echo '</td></tr>';
	
	echo '<tr><td>' ;
	echo '<label for="points">4. Point and Salary System? </label>' ;
	echo '<input type="radio" name="points" value="dk" checked>DraftKings</input>' ;
	echo '<input type="radio" name="points" value="vi">Victiv</input>' ;
	echo '<br><SPAN STYLE="color: gray; font-size: 10pt; margin-left: 16px">"Avg Pts/Rd" totals below are computed using only Eagles, Birdies, Pars, Bogeys, Doubles and Others. Holes in One and Double Eagles are counted as Eagles. </span>' ;
	echo '<br><SPAN STYLE="color: gray; font-size: 10pt; margin-left: 16px">Triples or Worse counted as Triples. <b>DraftKings and Victiv Salaries updated by 4pm EST on Mondays.</b></span>' ;
	echo '</td></tr>';
	
	echo '<tr><td>' ;
	echo '<label for="points">5. Only Show Players with Salaries Less Than?</label>' ;
	echo '<br><SPAN class="iq" STYLE="color: gray; font-size: 10pt; margin-left: 16px">' ;
	echo 'Check this box </span><input type="checkbox" name="filter" value="1"></input>' ;
	echo '<SPAN class="iq" STYLE="color: gray; font-size: 10pt; margin-left: 16px">' ;
	echo 'then use arrows to enter Max Salary for either DraftKings: <input size="4" min="1000" max="16000" step="500" value="7500" type="number" name="dmax" value="dmax"></input>  ' ;
	echo 'or Victiv: <input size="4" min="200000" max="1400000" step="10000" value="600000" type="number" name="vmax" value="vmax"></input></span>' ;
	echo '<br><SPAN STYLE="color: gray; font-size: 10pt; margin-left: 16px">For when you want to limit the players shown to just those underneath a certain salary figure.</span>' ;
	echo '</td></tr>';
	
	//echo '<br><label for="pretend">Pretend the U.S. Open is a British Open?</label>' ;
	//echo '<input type="radio" name="pretend" value="y">Yes</input>' ;
	//echo '<input type="radio" name="pretend" value="n" checked>No</input>' ;

	echo '<tr><td>' ;
	echo '<label for="perf">6. Display Performance Stats (GIR, Putts, Driving Dist, Fairways Hit) instead of Birdies, Bogeys, etc?</label>' ;
	echo '<input type="radio" name="perf" value="y">Yes</input>' ;
	echo '<input type="radio" name="perf" value="n" checked>No</input>' ;
	echo '<br><SPAN STYLE="color: gray; font-size: 10pt; margin-left: 16px">For when you want to search for players who fit a particular course or skill set.</span>' ;
	echo '</td></tr>';
	
	echo '<tr><td>' ;
	echo '<label for="perf">7. Sticky Headers</label>' ;
	echo '<input type="radio" name="sticky" value="2" checked>Yes</input>' ;
	echo '<input type="radio" name="sticky" value="1">No</input>' ;
	echo '<br><SPAN STYLE="color: gray; font-size: 10pt; margin-left: 16px">Do you want the headers to stick at the top of the page as you scroll?</span>' ;
	echo '<input type="hidden" name="charli" value='.$_GET['charli'].' >' ;
	echo '</td></tr>';
	

	echo '<tr><td>' ;
	echo '<input type="submit" name="submit" value="GO" class="submit">' ;
	echo '</td></tr>';
	
	echo '</form>' ;

	echo '</table>' ;
	echo '</div>' ;
			
	ja_section_wrap( 'close' );

	}

if ($_GET['tid'] > 1) {

//	wp_enqueue_script( 'tablesort',    CHILD_URL . '/lib/js/stupidtable-new.js',          array( 'jquery' ), CHILD_THEME_VERSION, false );

	$tid = $_GET['tid'] ;
	$tyear = substr($tid, -4);
	$eid = substr($tid,0,strlen($tid)-4) ;
	//echo $tid.'-'.$eid.'-'.$tyear ;
	
	//echo $_GET['pretend'].'-'.$eid.'-'.$tyear ;
	//if ($_GET['pretend'] == 'y' && $eid == 24) $eid = 28 ;
	//echo $_GET['pretend'].'-'.$eid.'-'.$tyear ;

	
	$pts = $_GET['points'] ;
	$rounds = $_GET['rounds'] ;
	$dmax = $_GET['dmax'] ;
	$vmax = $_GET['vmax'] ;
	$filter = $_GET['filter'] ;
	if ($filter == '') $filter = 0 ;
	
	ja_section_wrap();
	
	$dblink = mysql_connect( 'localhost', 'golfstat_query', 'gstt2qry' );
	mysql_select_db( "golfstat_main", $dblink );
	$q1 = 'SELECT name,dates,players FROM field WHERE tid = '.$_GET['tid'] ;
	//echo $q1;
	$results = mysql_query($q1) or die (mysql_error()." 7 \r\n");
	$r1 = mysql_fetch_assoc( $results ) ;
	$tname = $r1['name'] ;
	
	if ($pts == 'dk') { $dfs = 'DraftKings' ; if ($filter == 1) $smax = $dmax; }
	if ($pts == 'vi') { $dfs = 'Victiv' ; if ($filter == 1) $smax = $vmax ; }
	
	$pagetitle = 'GOLFstats IQ - Prior Tournaments Report for '.$tname ;

	if ($_GET['nohdr'] == 1) ja_search_results_heading($pagetitle, false, false);
	else ja_search_results_heading($pagetitle, false, true, 'new');

	$tabletext = '<div class="ryder-table-area">' ;
	echo '<div class="alert alert-announce stats-alert"><b>Report for <u>'.$r1['name'].'</u> (current field):</b><br>Past <u>'.$_GET['years'].'</u> Years of Tournament; Past <u>'.$_GET['weeks'].'</u> Weeks on Tour<br>Avg Pts/Rd computed using <u>'.$dfs.'</u> Point Values, Salary Max: <u>'.$smax.'</u>
	<br>Click any column title to sort, twice to reverse order. "CM/T" = Cuts Made / Tournaments Played. Avg Finish includes Cuts (counted as 100). 
	<br>E=Eagles, Bi=Birdies, P=Pars, Bg=Bogeys,D=Doubles, O=Others. Hole in Ones and Double Eagles are counted as Eagles. Triples or Worse counted as Triples.
	<br>DraftKings point values are approximate. We currently do not count the players finish, rounds under 70, bogey streak, 3 birdies or better, or hole in ones in the DraftKings point numbers.
	<br><b>DraftKings FPPG = Fantasy Points Per Game (average DraftKings Points per Tournament) - received from DraftKings every Monday.</b>
	<br>Tournaments used for previous N weeks listed at the bottom of the table. Combined columns take the average of the Years and Weeks values.
	<br>Results deemed accurate but not guaranteed, and are provided for informational purposes only. Use at your own risk.' ;
	if ($_GET['nohdr'] == 1) echo '<br><a href="http://www.golfstats.com/nabil/?tid=1">Go to IQ Home Page</a>' ;
	echo '</div>' ;
	

	//echo '<table class="gs-table table-sortable"><thead><tr><th colspan="4">The Big Table Header</th></tr><tr><th class="clickable" data-sort="string">Letter</td><th  class="clickable" colspan="2" data-sort="string">colspan=2</th><th  class="clickable" data-sort="int">Number</td></tr></thead><tbody><tr><td>def</td><td>X</td><td>9</td><td>1</td></tr><tr><td>abc</td><td>Z</td><td>8</td><td>2</td></tr><tr><td>bcd</td><td>Y</td><td>7</td><td>0</td></tr></tbody></table><br>' ;
	//$tabletext .= '<table id="ranking_table" class="table gs-table ryder-table ranking-table table-sortable ">' ;
	
	// decide if we want sticky headers
	if ($_GET['sticky'] <> 1) $sticky = " sticky-header" ; else $sticky = " " ;
	
	$tabletext .= '<table class="table table-bordered table-sortable gs-table ranking-table gs-table2 '.$sticky.'">' ;

	$tabletext .= '<thead>' ;
	
	
	$tname = $r1['name'] ;
	//if ($_GET['pretend'] == 'y') $tname = 'British Open' ;
	$tabletext .= '<tr><td><td colspan="2">from DraftKings</td><td colspan="2">Combined</td><td colspan="10">Previous '.$_GET['years'].' Years for '.$tname.'</td><td colspan="10">Previous '.$_GET['weeks'].' Weeks on Tour*</td></tr>' ;
	
	if ($pts == 'dk') $stitle = 'Draft Kings' ; else $stitle = 'Victiv' ;
	
	if ($_GET['perf'] == 'y') {
	$tabletext .= '<tr> 
	<th class="clickable" ' . gs_header_sort_type( 'str' ) . '>Player</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>'.$stitle.' Salary</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>DraftKings FPPG</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>Avg Fin</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>---</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>CM/T</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Rds</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>Avg Fnsh</th>
	<th id="this_one" class="clickable" ' . gs_header_sort_type( 'float' ) . '>---</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Avg Fways Hit per Rnd</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Avg Drive Dist</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Avg GIR per Rnd</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>Avg Putts per Rnd</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>---</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>---</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>CM/T</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Rds</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>Avg Fnsh</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>---</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Avg Fways Hit per Rnd</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Avg Drive Dist</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Avg GIR per Rnd</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>Avg Putts per Rnd</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>---</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>---</th>
	</tr>' ;
	
	if (is_super_admin()) $tabletext .=  '
	<tr><td colspan="25">
	<b><font style="color:orange">TEAMBUILDER:</font> Jason Day</b> (12,100), <b>Patrick Reed</b> (8400), <b>Brooks Koepka</b> (8100), <b>Shane Lowery</b> (7100) : <b>Total 35,700 (<font style="color:green">14,300</font></b>)
	</td></tr>' ;

	$tabletext .= '</thead><tbody>' ; 
	}
	else { $tabletext .= '<tr> 
	<th class="clickable" ' . gs_header_sort_type( 'str' ) . '>Player</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>'.$stitle.' Salary</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>Draft Kings FPPG</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '> Avg Fin</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>'.$stitle.' Avg Pts (approx)</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>CM/T</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Rds</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>Avg Fnsh</th>
	<th id="this_one" class="clickable" ' . gs_header_sort_type( 'float' ) . ' >'.$stitle.' Avg Pts/Rd (approx)</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>E</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Bi</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>P</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Bg</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>D</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>O</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>CM/T</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Rds</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>Avg Fnsh</th>
	<th class="clickable" ' . gs_header_sort_type( 'float' ) . '>'.$stitle.' Avg Pts/Rd (approx)</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>E</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Bi</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>P</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>Bg</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>D</th>
	<th class="clickable" ' . gs_header_sort_type( 'int' ) . '>O</th>
	</tr>' ; 
	
	if (is_super_admin()) $tabletext .=  '
	<tr><td colspan="25">
	<b><div style="text-align:left;"><font style="color:orange">TEAMBUILDER:</font> Jason Day</b> (12,100), <b>Patrick Reed</b> (8400), <b>Brooks Koepka</b> (8100), <b>Shane Lowery</b> (7100) : <b>Total 35,700 (<font style="color:green">14,300</font></b>)</p>
	</td></tr>' ;

	$tabletext .= '</thead><tbody>' ; 
	
	}

	// put up banner if not ultimate
	if (!gs_is_ultimate() && $_GET['charli'] <> '21') ultimate_block() ;

	$noplayers = '' ;
	$field = explode(',',$r1['players']) ;
	foreach ($field as $player) {
		// get pid
		$dblink = mysql_connect( 'localhost', 'golfstat_query', 'gstt2qry' );
		mysql_select_db( "golfstat_main", $dblink );
		$q1 = 'SELECT pid FROM players WHERE pname = "'.$player.'" || paltname ="'.$player.'" || paltname2="'.$player.'" || paltname3="'.$player.'" LIMIT 1' ;
		//echo $q1;
		$results = mysql_query($q1) or die (mysql_error()." 8 \r\n");
		$r2 = mysql_fetch_assoc( $results ) ;
		$pid = $r2['pid'] ;
		if ($pid == '') {
		if ($noplayers <> '') $noplayers .= ', ' ;
		$noplayers .= $player ;
		}

		//if (is_super_admin()) echo $player.'-'.$pid.'<br>';
		
		// create where string for previous 'years' tournaments
		$count = $_GET['years'] ; $where ='' ;
		//echo $count.':'.$tyear ;
		for ($i=$tyear-$count;$i<=$tyear-1;$i++) {
			if ($where <> '') $where .= ' || ' ;
			$where .= 'tid = '.$eid.$i ;
			}
		$where = '( '.$where.' )' ;
		
		// setup database access
		$dblink = mysql_connect( 'localhost', 'golfstat_query', 'gstt2qry' );
		mysql_select_db( "golfstat_main", $dblink );

		// compute cuts made and tournaments played
		// $q1 = 'SELECT place FROM boxscores WHERE name = "'.$player.'" && '.$where ;
		$q1 = 'SELECT place FROM boxscores WHERE pid = "'.$pid.'" && '.$where ;
		//if (is_super_admin()) echo $q1.'<br>';
		$results = mysql_query($q1) or die (mysql_error()." a \r\n");
		$t_played = mysql_num_rows($results) ;
		$c_made = 0 ;
		while ( $r1 = mysql_fetch_assoc( $results ) ) {
			if ($r1['place'] < 100) $c_made++ ; }
		
		// now get rest of values
		//$q1 = 'SELECT SUM(rds) as rds, SUM(eagles) as E, SUM(birdies) as B, SUM(pars) as P, SUM(bogeys) as BOG, SUM(doubles) as D, SUM(others) as O, AVG(place) as pl FROM boxscores WHERE name = "'.$player.'" && '.$where ;
		//if ($_GET['perf'] == 'y') {
		//	$q1 = 'SELECT AVG(place) as pl, SUM(rds) as rds, SUM(fh) as r_fh, AVG(distance) as r_dis , SUM(gir) as r_gir , SUM(putts_per) as r_putts FROM boxscores WHERE name = "'.$player.'" && '.$where ;
		//	$perf = 'y' ;
		//	}

		// now get rest of values with pid
		$q1 = 'SELECT SUM(rds) as rds, SUM(eagles) as E, SUM(birdies) as B, SUM(pars) as P, SUM(bogeys) as BOG, SUM(doubles) as D, SUM(others) as O, AVG(place) as pl FROM boxscores WHERE pid = "'.$pid.'" && '.$where ;
		if ($_GET['perf'] == 'y') {
			$q1 = 'SELECT AVG(place) as pl, SUM(rds) as rds, SUM(fh) as r_fh, AVG(distance) as r_dis , SUM(gir) as r_gir , SUM(putts) as r_putts FROM boxscores WHERE pid = "'.$pid.'" && '.$where ;
			$perf = 'y' ;
			}
			
		//echo $q1.'<br>';
		$results = mysql_query($q1) or die (mysql_error()." 3 \r\n");
		$r3 = mysql_fetch_assoc( $results ) ;
		// compute points
		if ($pts == 'dk') { $pe=8;$pb=3;$pp=0.5;$pbo=-0.5;$pd=-1;$po=-1;}
		if ($pts == 'vi') { $pe=9;$pb=5;$pp=2;$pbo=-1;$pd=-4;$po=-7;}
		$points = 0 ;
		if ($r3['rds'] > 0) $points = number_format((($r3['E']*$pe)+($r3['B']*$pb)+($r3['P']*$pp)+($r3['BOG']*$pbo)+($r3['D']*$pd)+($r3['O'])*$po) / $r3['rds'],2) ;
		
		// save avg pts and avg finish for later
		$yap = $points;
		$yaf = $r3['pl'];
		

		// get date for this tournament
		//$q1 = 'SELECT tdate FROM future WHERE eid = '.$eid ;
		//echo $q1;
		//$gt = mysql_query( $q1, $dblink );
		//$rt = mysql_fetch_assoc( $gt ) ;
		//$tdate = $rt['tdate'] ;

		// get date for this tournament
		// assuming format of March 5 - 9, 2015
		// need to handle format of Feburary 29 - March 1, 2015
		$q1 = 'SELECT dates FROM field WHERE tid = '.$_GET['tid'] ;
		$results = mysql_query($q1) or die (mysql_error()." 7 \r\n");
		$r1 = mysql_fetch_assoc( $results ) ;
		$tdate5 = str_replace("-","",$r1['dates']) ;
		$tdate5 = str_replace(",","",$tdate5) ;
		$tdate5 = str_replace("st","",$tdate5) ;
		$tdate6 = explode(" ",$tdate5) ;
		$mydate = $tdate6[0].'-1-1' ;
		//if (is_super_admin()) { echo '<br> tdate5:'.$tdate5.'<br>tdate6:' ; print_r ($tdate6) ; }
		if ($tdate6[5] <> '')
			$tdate = $tdate6[5].'-'.date("n",strtotime($mydate)).'-'.$tdate6[4] ;
			else $tdate = $tdate6[4].'-'.date("n",strtotime($mydate)).'-'.$tdate6[3] ;
		$tdate = str_replace("th","",$tdate) ;
		
		//$mydate = $tdate6[0].'-1-1' ;
		//if (is_super_admin()) echo '<br>ecp:'.$mydate.' date n: '.date('n',strtotime($mydate)) ;

		// create where string for previous 'weeks' tournaments
		$minus9 = date( 'Y-m-d', strtotime( " - ".$_GET['weeks']." weeks ", strtotime($tdate)) );
		
		//if (is_super_admin()) echo '<br> tdate: '.$tdate.' and minus '.$_GET['weeks'].' weeks: '.$minus9;
		
		// max number of tournaments *2 (for PGA TOUR and European Tour)
		$maxt   = ' LIMIT '.$_GET['weeks']*2;
		
		// lookup tour for this EID
		$q1 = 'SELECT ttour FROM tournaments WHERE eventid = '.$eid.' LIMIT 1' ;
		$g1 = mysql_query( $q1, $dblink );
		$r1 = mysql_fetch_assoc( $g1 )  or die (mysql_error()." 4 \r\n");
		$tour = $r1['ttour'] ;
		switch ($tour) {
			case "Both" :
			case "Majors" :
			case "PGA" : 
			case "Other" :
			case "European" : $twhere = "(t.ttour = 'PGA' || t.ttour = 'European' || t.ttour = 'Majors' || t.ttour = 'Both' || t.ttour = 'Other')" ; break ;
			case "LPGA" : $twhere = "(t.ttour = 'LPGA')" ; break ;
			case "Champions" : $twhere = "(t.ttour = 'Champions')" ; break ;
			default: $twhere = "(t.ttour = 'PGA' || t.ttour = 'European' || t.ttour = 'Majors' || t.ttour = 'Both' || t.ttour = 'Other')" ; break ;
			}
		
		//$qt = "SELECT t.tid, t.tname, t.tyear, t.tdate, t.ttour FROM tournaments as t WHERE (t.ttour = 'PGA' || t.ttour = 'Majors' || t.ttour = 'Both') && t.tdate < '".$tdate. "' && t.tdate >= '" .$minus9. "' ORDER BY t.tdate DESC" . $maxt;
		$qt = "SELECT t.tid, t.tname, t.tyear, t.tdate, t.ttour FROM tournaments as t WHERE ".$twhere." && t.tdate < '".$tdate. "' && t.tdate >= '" .$minus9. "' ORDER BY t.tdate DESC" . $maxt;
		//if (is_super_admin()) echo $qt.$tour;
		$gt = mysql_query( $qt, $dblink ) or die (mysql_error()." cc \r\n");

		$count = $_GET['weeks'] ; $where ='' ; $tnames = '' ;
		//if (is_super_admin()) echo $count.':'.$tyear.';'.mysql_num_rows($results) ;
		//if (is_super_admin() && mysql_num_rows($results) == 1) echo 'results = 1 and tid = '.$rt['tid'] ; 
		while ( $rt = mysql_fetch_assoc( $gt ) ) {
			if ($where <> '') $where .= ' || ' ;
			$where .= 'tid = '.$rt['tid'] ;
			if ($tnames <> '') $tnames .= ', ' ;
			$tnames .= $rt['tname'].'('.$rt['ttour'].' Tour)' ;
			}
		$where = ' ( '.$where.' ) ' ;
		//if (is_super_admin()) echo $where.'-'.$tnames ;

		// setup database access
		$dblink = mysql_connect( 'localhost', 'golfstat_query', 'gstt2qry' );
		mysql_select_db( "golfstat_main", $dblink );

		if ($pid > 0) {
		// get victiv and draftkings salary
		$q4 = 'SELECT vsalary, dsalary FROM players WHERE pid = "'.$pid.'"' ;
		// echo $q1.'<br>';
		$r4 = mysql_query($q4) or die (mysql_error()." 5 \r\n");
		$d4 = mysql_fetch_assoc ($r4) ;
		$dsalary = $d4['dsalary'] ;
		//$dsalary = '' ;
		$vsalary = $d4['vsalary'] ;
		
		// get draftkings FPPG
		// what is weeknum of tournament?
		
		$weeknum = date("W",strtotime($tdate)) ;
		$q4 = 'SELECT fppg FROM dk_salary WHERE pid = '.$pid.' AND weeknum = '.$weeknum ;
		//if (is_super_admin()) echo $q4.'<br>';
		$r4 = mysql_query($q4) or die (mysql_error()." 5 \r\n");
		$d4 = mysql_fetch_assoc ($r4) ;
		$dfppg = $d4['fppg'] ;
		if ($dfppg == '') $dfppg = 0.0 ;
		}
		
		// only do these queries if there are tournaments in the past N weeks
		if ($where <> ' (  ) ') {
		
		// get boxscores for these previous weeks tournament
		
		// compute cuts made and tournaments played
		// if (is_super_admin()) echo 'pid:'.$player.$pid;
		// $q1 = 'SELECT place FROM boxscores WHERE name = "'.$player.'" && '.$where ;
		$q1 = 'SELECT place FROM boxscores WHERE pid = "'.$pid.'" && '.$where ;
		//if (is_super_admin()) echo $q1.'<br>';
		$results = mysql_query($q1) or die (mysql_error()." b \r\n");
		$t_played1 = mysql_num_rows($results) ;
		$c_made1 = 0 ;
		while ( $r1 = mysql_fetch_assoc( $results ) ) {
			if ($r1['place'] < 100) $c_made1++ ; }

		
		// now get rest of values
		//$q1 = 'SELECT tname, SUM(rds) as rds, SUM(eagles) as E, SUM(birdies) as B, SUM(pars) as P, SUM(bogeys) as BOG, SUM(doubles) as D, SUM(others) as O, AVG(place) as pl FROM boxscores WHERE name = "'.$player.'" && '.$where ;
		//if ($_GET['perf'] == 'y') {
		//	$q1 = 'SELECT tname, AVG(place) as pl, SUM(rds) as rds, SUM(fh) as r_fh, AVG(distance) as r_dis , SUM(gir) as r_gir , SUM(putts_per) as r_putts FROM boxscores WHERE name = "'.$player.'" && '.$where ;
		//	}

		// now get rest of values with PID
		$q1 = 'SELECT tname, SUM(rds) as rds, SUM(eagles) as E, SUM(birdies) as B, SUM(pars) as P, SUM(bogeys) as BOG, SUM(doubles) as D, SUM(others) as O, AVG(place) as pl FROM boxscores WHERE pid = "'.$pid.'" && '.$where ;
		if ($_GET['perf'] == 'y') {
			$q1 = 'SELECT tname, AVG(place) as pl, SUM(rds) as rds, SUM(fh) as r_fh, AVG(distance) as r_dis , SUM(gir) as r_gir , SUM(putts) as r_putts FROM boxscores WHERE pid = "'.$pid.'" && '.$where ;
			}
			
		//$q1 = 'SELECT tname, SUM(rds) as rds, SUM(eagles) as E, SUM(birdies) as B, SUM(pars) as P, SUM(bogeys) as BOG, SUM(doubles) as D, SUM(others) as O, AVG(place) as pl FROM boxscores WHERE pid = "'.$pid.'" && '.$where ;
		//if (is_super_admin()) echo $q1.'<br>';
		$results = mysql_query($q1) or die (mysql_error()." 6 \r\n");
		$r4 = mysql_fetch_assoc( $results ) ;

		// end of if where <> '  ( )  ' ;
		} else { $r4 = array() ; $r4['rds'] = 0 ; $r4['pl'] = 0 ; }
		
		// now compute points
		if ($pts == 'dk') { $pe=8;$pb=3;$pp=0.5;$pbo=-0.5;$pd=-1;$po=-1;$salary=$dsalary;$smax=$dmax;}
		if ($pts == 'vi') { $pe=9;$pb=5;$pp=2;$pbo=-1;$pd=-4;$po=-7;$salary=$vsalary;$smax=$vmax;}
		$wpoints = 0 ;
		if ($r4['rds'] > 0) $wpoints = number_format((($r4['E']*$pe)+($r4['B']*$pb)+($r4['P']*$pp)+($r4['BOG']*$pbo)+($r4['D']*$pd)+($r4['O'])*$po) / $r4['rds'],2) ;
		
		// what is the divisor for the averages?
		$divisor = 0;
		if (($r4['pl'] > 0) && ($r3['pl'] > 0)) $divisor = 2;
		if (($r4['pl'] == 0) && ($r3['pl'] == 0)) $divisor = 1;
		if (($r4['pl'] > 0) && ($r3['pl'] == 0)) $divisor = 1;
		if (($r4['pl'] == 0) && ($r3['pl'] > 0)) $divisor = 1;

		// for date sort value on finishes
		if ($r3['pl'] == 0) $dsf = 0 ; else $dsf = 1000 - number_format($r3['pl'],1) ;
		if ($r4['pl'] == 0) $dsf1 = 0 ; else $dsf1 = 1000 - number_format($r4['pl'],1) ;
		if (($r4['pl']+$yaf) == 0) $dsf2 = 0 ; else $dsf2 = 1000 - number_format(($r4['pl']+$yaf)/$divisor,1) ;
		
		// data sort value for avg putts per round
		if ($r3['r_putts'] == 0) $putt3 = 0 ; else $putt3 = 1000 - number_format($r3['r_putts'],1) ;
		if ($r4['r_putts'] == 0) $putt4 = 0 ; else $putt4 = 1000 - number_format($r4['r_putts'],1) ;

		if ( $r4['rds'] == '') $r4['rds'] = 0 ;
		// if (is_super_admin()) echo $r4['rds'].'-'.$rounds.'<br>';
		
		//if (is_super_admin()) {
		//	if ($player == 'Brian Harman') echo $player.'-'.$r3['r_gir'].'-'.$r3['rds'] ;
		//	}
		

		if ($_GET['perf'] == 'y') {
		// add to table row for previous years
		if ($r4['rds'] == 0) $r4['rds'] = 1 ; if ($r3['rds'] == 0) $r3['rds'] = 1 ;
		$tabletext .= '<tr><td>'.player_bio($player, $tname, $tyear).'</td><td>'.$salary
		.'</td><td>'.$dfppg.'</td><td data-sort-value="'.$dsf2.'">'.number_format(($r4['pl']+$yaf)/$divisor,1)
		.'</td><td></td><td>'.$c_made.'/'.$t_played.'</td><td>'.$r3['rds'].'</td><td data-sort-value="'.$dsf.'">'.number_format($r3['pl'],1)
		.'</td><td></td><td>'.number_format($r3['r_fh']/$r3['rds'],1).'</td><td>'.number_format($r3['r_dis'],1).'</td><td>'.number_format($r3['r_gir']/$r3['rds'],1)
		.'</td><td>'.number_format($r3['r_putts']/$r3['rds'],1).'</td><td></td><td></td>' ;
		// add to table row for previous weeks
		$tabletext .= '<td>'.$c_made1.'/'.$t_played1.'</td><td>'.$r4['rds'].'</td><td data-sort-value="'.$dsf1.'">'.number_format($r4['pl'],1)
		.'</td><td></td><td>'.number_format($r4['r_fh']/$r4['rds'],1).'</td><td>'.number_format($r4['r_dis'],1).'</td><td>'.number_format($r4['r_gir']/$r4['rds'],1)
		.'</td><td>'.number_format($r4['r_putts']/$r4['rds'],1)
		.'</td><td></td><td></td></tr>';
		}
		else {
		if ( ($r4['rds'] >= $rounds) && ($filter == 0 || ($filter == 1 && $salary <= $smax )))
			{
			// add to table row for previous years
			$tabletext .= '<tr><td>'.player_bio($player, $tname, $tyear).'</td><td>'.$salary
			.'</td><td>'.$dfppg.'</td><td data-sort-value="'.$dsf2.'">'.number_format(($r4['pl']+$yaf)/$divisor,1)
			.'</td><td>'.number_format(($wpoints+$yap)/$divisor,1)
			.'</td><td>'.$c_made.'/'.$t_played.'</td><td>'.$r3['rds'].'</td><td data-sort-value="'.$dsf.'">'.number_format($r3['pl'],1)
			.'</td><td>'.$points.'</td><td>'.$r3['E'].'</td><td>'.$r3['B'].'</td><td>'.$r3['P']
			.'</td><td>'.$r3['BOG'].'</td><td>'.$r3['D'].'</td><td>'.$r3['O'].'</td>' ;
			// add to table row for previous weeks
			$tabletext .= '<td>'.$c_made1.'/'.$t_played1.'</td><td>'.$r4['rds'].'</td><td data-sort-value="'.$dsf1.'">'.number_format($r4['pl'],1)
			.'</td><td>'.$wpoints.'</td><td>'.$r4['E'].'</td><td>'.$r4['B'].'</td><td>'.$r4['P'].'</td><td>'.$r4['BOG']
			.'</td><td>'.$r4['D'].'</td><td>'.$r4['O'].'</td></tr>';
			}
			}
		}
	
	$tabletext .= '</tbody></table>' ;
	if ($tnames <> '') 	$tabletext .= 'Note: Tournaments included in last '.$_GET['weeks'].' weeks are '.$tnames.'<br>* = Any tournament on PGA or European Tour</div>' ;
	else $tabletext .= 'Note: No tournaments played on tour in the past '.$_GET['weeks'].' weeks</div>' ;
	echo $tabletext ;
	
	// mail notice
	$to = "ed@edpattermann.com";
	$subject = "GOLFstats iQ Report";
	$header = "From:\"GolfStats\" ed@golfstats.com \r\nMIME-Version: 1.0\r\nContent-type: text/html \r\n";
	$message = 'User: ' . mm_member_data(array("name"=>"firstName")) . ' ' . mm_member_data(array("name"=>"lastName")) . ' (' . mm_member_data(array("name"=>"email")) . ')'
	.'<br>Tournament: '.$tname
	.'<br>Years: '.$_GET['years']
	.'<br>Weeks: '.$_GET['weeks']
	.'<br>Points: '.$dfs
	.'<br>Filter: '.$filter.' - Salary Max: '.$smax
	.'<br>Rounds Min: '.$rounds
	.'<br>Using Performance Data?: '.$_GET['perf']
	.'<br>NoPlayers: '.$noplayers
	.'<br><br>IP Address: ' . get_client_ip() . '<br>' . 'Hostname: ' . gethostbyaddr(get_client_ip()) . '<br><br>' ;
	
	//if (mm_member_data(array("name"=>"email")) <> 'ed@edpattermann.com' && mm_member_data(array("name"=>"email")) <> '') $retval = mail($to,$subject,$message,$header);
	
	//echo $tour ;
	$sendwarningemail = 0 ;
	if ($tour == 'PGA') $sendwarningemail = 1;
	if ($tour == 'Majors') $sendwarningemail = 1;
	if ($tour == 'Both') $sendwarningemail = 1;
	//echo $sendwarningemail ;
	
	if ($noplayers <> '' && $sendwarningemail == 2) {
		// mail notice of missing players
		$to = "golfersal@aol.com";
		$subject = "GOLFstats iQ - Missing Players";
		$header = "From:\"GolfStats\" ed@golfstats.com \r\nMIME-Version: 1.0\r\nContent-type: text/html\r\nBcc: edp@windermerenaz.com \r\n";
		$message = 'Sal, <br><br>These players are in the FIELD for '.$tname.' (tour='.$tour.'), but are not in the PLAYERS db (either as a pname, or any altname)'
		.'<br><br>'.$noplayers.'<br><br>They need to be added to the PLAYERS database exactly as spelled above. Thank you!<br><br>-- Ed<br><br>Note: This is an automated message from the GOLFstats IQ Page' ;
		$retval = mail($to,$subject,$message,$header);
		}
	
	ja_section_wrap( 'close' );

	}
	

// for floating table headers
// now in lib/global.js
//<script>
//$('.sticky-header').stickyTableHeaders();
//</script>


};

add_action( 'genesis_loop', 'ja_predict_content' );

add_action( 'wp_enqueue_scripts', 'jsrow_enqueue_script' );
function jsrow_enqueue_script() {
    wp_enqueue_script( 'jsrow', get_stylesheet_directory_uri() . '/jsrow.js', array( 'jquery' ) );
    wp_enqueue_style( 'jsrow', get_stylesheet_directory_uri() . '/jsrow.css' );
}

genesis();